import 'package:flutter/material.dart';
import 'screens/login/auth.dart';
import 'screens/root/root.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wander',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.grey,
          primaryTextTheme: TextTheme(
              title: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontSize: 13.0),
              body1: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontSize: 12.0))),
      home: RootPage(auth: new Auth()),
    );
  }
}
