import 'package:flutter/material.dart';
import '../login/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.user, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final FirebaseUser user;
  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final appBarTextStyle =
      TextStyle(color: Colors.white, fontFamily: 'Pacifico', fontSize: 14.0);

  final headerTextStyle = TextStyle(
      color: Colors.white,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      fontSize: 14);

  final bodyTextStyle = TextStyle(
      color: Colors.white,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      fontSize: 12);

  final mutedTextStyle = TextStyle(
      color: Color.fromRGBO(180, 180, 180, 1),
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w500,
      fontSize: 11.5);

  final cardColor = Color.fromRGBO(46, 47, 65, 1);

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  Widget postCard() {
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 5.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        elevation: 5.0,
        color: cardColor,
        child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
            child: Row(
              children: <Widget>[
                Container(                  
                  height: 100,
                  child: Image(
                      image: AssetImage("assets/osaka2.jpg"),
                      fit: BoxFit.contain),
                ),
                SizedBox(width: 15.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("Post Title", style: bodyTextStyle),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text("by Al Yin", style: mutedTextStyle),
                  ],
                ),
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(38, 41, 56, 1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(47, 49, 66, 1),
        // title: Text("Welcome " + widget.user.email),
        titleSpacing: 0.0,
        actions: <Widget>[
          FlatButton(
              child: Text('feed', style: appBarTextStyle), onPressed: () {}),
          FlatButton(
              child: Text('people', style: appBarTextStyle), onPressed: () {}),
          FlatButton(
              child: Text('events', style: appBarTextStyle), onPressed: () {}),
          FlatButton(
              child: Icon(
                Icons.account_circle,
                color: Colors.white,
              ),
              onPressed: signOut)
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20),
            Text("College Park, Maryland", style: headerTextStyle),
            Text("42 lost nearby", style: mutedTextStyle),
            SizedBox(height: 20),
            postCard(),
            postCard()
          ],
        ),
      ),
    );
  }
}
