import 'package:flutter/material.dart';
import './auth.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.loginCallback});

  final BaseAuth auth;
  final VoidCallback loginCallback;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style =
      TextStyle(color: Colors.white, fontFamily: 'Montserrat', fontSize: 12.0);

  final baseColor = Color.fromRGBO(38, 41, 56, .95);
  final highlightColor = Color.fromRGBO(57, 58, 77, 1);
  final focusColor = Color.fromRGBO(62, 64, 84, 1);

  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  String _errorMessage;

  bool _isLoginForm;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
    });
    if (validateAndSave()) {
      String userId = "";
      try {
        if (_isLoginForm) {
          userId = await widget.auth.signIn(_email, _password);
          print('Signed in: $userId');
        } else {
          userId = await widget.auth.signUp(_email, _password);
          print('Signed up user: $userId');
        }

        if (userId.length > 0 && userId != null && _isLoginForm) {
          widget.loginCallback();
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoginForm = true;
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }

  Widget userField() {
    return TextFormField(
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      style: style,
      decoration: new InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color.fromRGBO(255, 255, 255, .2)),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color.fromRGBO(255, 255, 255, .6)),
        ),
        hintText: _isLoginForm ? 'Email address' : 'Valid email address',
        hintStyle: TextStyle(color: Colors.white),
        labelText: 'Username',
        labelStyle: TextStyle(color: Colors.white),
        prefixIcon: const Icon(
          Icons.person,
          color: Colors.white,
        ),
      ),
      validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
      onSaved: (value) => _email = value.trim(),
    );
  }

  Widget passwordField() {
    return TextFormField(
      maxLines: 1,
      obscureText: true,
      style: style,
      autofocus: false,
      decoration: new InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color.fromRGBO(255, 255, 255, .2)),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color.fromRGBO(255, 255, 255, .6)),
        ),
        labelText: 'Password',
        labelStyle: TextStyle(color: Colors.white),
        prefixIcon: const Icon(
          Icons.lock,
          color: Colors.white,
        ),
      ),
      validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
      onSaved: (value) => _password = value.trim(),
    );
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 10.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w400),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget loginButton() {
    return Material(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      elevation: 5.0,
      color: highlightColor,
      child: MaterialButton(
        highlightColor: focusColor,
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: validateAndSubmit,
        child: Text(_isLoginForm ? 'WANDER' : 'SIGN UP',
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget signupLink() {
    return FlatButton(
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      onPressed: toggleFormMode,
      child: Text(
          _isLoginForm ? 'Create an account' : 'Have an account? Sign in',
          textAlign: TextAlign.center,
          style: style.copyWith(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 10)),
    );
  }

  Widget loginCard() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      elevation: 10.0,
      color: baseColor,
      child: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Column(
                    children: <Widget>[
                      userField(),
                      SizedBox(height: 10.0),
                      passwordField(),
                    ],
                  )),
              SizedBox(
                height: 15.0,
              ),
              showErrorMessage(),
              SizedBox(
                height: 15.0,
              ),
              loginButton(),
              signupLink(),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/osaka2.jpg"), fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Container(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: Stack(
                  children: <Widget>[loginCard()],
                )),
          ),
        ),
      ),
    );
  }
}
